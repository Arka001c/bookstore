package in.arka.onlinebookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.arka.onlinebookstore.entity.Book;

public interface BookRepo extends JpaRepository<Book, Long>{			// repository for Book.java

}
