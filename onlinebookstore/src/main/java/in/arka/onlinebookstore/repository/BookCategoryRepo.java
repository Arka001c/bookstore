package in.arka.onlinebookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.arka.onlinebookstore.entity.BookCategory;

public interface BookCategoryRepo extends JpaRepository<BookCategory, Long>{		// repository for BookCategory.java

}
